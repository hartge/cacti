#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

%:
	dh ${@} --with linktree

TDIR:=$(CURDIR)/debian/cacti
SHAREDIR:=${TDIR}/usr/share/cacti
SITEDIR:=${SHAREDIR}/site
LIBDIR:=${TDIR}/var/lib/cacti

export HOME=$(CURDIR)/tmp-home

override_dh_auto_configure:
	# make sure that we catch it when cli-include-path.patch is not up-to-date
	if grep -r -E '\.\./(include|repair)' cli | grep -v /site/; then \
		echo " ^ fix above include paths in cli directory ^" && false; \
	fi
	dh_auto_configure

override_dh_clean:
	# make sure that we catch translation updates
	debconf-updatepo

	# make sure we catch new themes (that must be stripped from source and
	# linked in)
	for theme in classic dark modern paper-plane paw ; do \
		if [ -f include/themes/$${theme}/jquery-ui.css ] ; then \
			echo "New theme '$${theme}' found. Add it to d/copyright, d/cacti.links and d/cacti.linktrees" \
			&& return 1 ; \
		fi ; \
	done

	dh_clean

override_dh_auto_build:
	# Upstream doesn't provide the source of the documentation in the tar, so
	# we include that via a second tar ball in the release and build it from
	# there.
	cd docs-source ; bash -ex bin/build_html_docs.sh

	# Generate all translations from source
	cd locales/po ; for file in *.po ; do msgfmt -o ../LC_MESSAGES/$${file%.po}.mo $${file} ; done

	dh_auto_build

override_dh_fixperms:
	# normalize permissions in the site directory
	find ${SITEDIR}/ -type d -exec chmod 755 {} \;
	find ${SITEDIR}/ -type f -exec chmod 644 {} \;
	find ${SITEDIR}/scripts -type f -exec chmod 755 {} \;
	# and then fix the special case files
	find ${SITEDIR}/scripts -type f -name '*.php' -exec chmod 644 {} \;
	find ${SHAREDIR}/cli -type f -print0 | xargs -0 file | \
		grep -e /usr/bin/php -e "PHP script, ASCII text executable" | \
		cut -d: -f1 | xargs chmod 755

	dh_fixperms
	# Do this after the call to dh_fixperms to not miss files unintentionally
	find ${LIBDIR}/* -exec chown root:www-data {} \;
	find ${LIBDIR}/* -type d -exec chmod 770 {} \;

override_dh_link:
	# Because upstream has an unconventional way of naming locale file, and
	# assumes jquery-ui files with these names to detect locale support, lets
	# generate links for all cacti supported languages.
	mkdir -p ${SITEDIR}/include/js/LC_MESSAGES
	cd locales/po ; \
	for file in *.po ; do \
		language=$$(grep '^"Language: ' $${file} | cut -f2 -d' ' | cut -f1 -d"\\" | sed s/_/-/) ; \
		if [ -z "$${language}" ] ; then echo "$${file} doesn't define its language" ; return 1 ; fi ; \
		if [ -f /usr/share/javascript/jquery-ui/ui/i18n/datepicker-$${language}.min.js ] ; then \
			ln -s \
				/usr/share/javascript/jquery-ui/ui/i18n/datepicker-$${language}.min.js \
				${SITEDIR}/include/js/LC_MESSAGES/jquery.ui.datepicker-$${file%.po}.js ; \
		else \
			language=$${language%-*} ; \
			if [ -f /usr/share/javascript/jquery-ui/ui/i18n/datepicker-$${language}.min.js ] ; then \
				ln -s \
					/usr/share/javascript/jquery-ui/ui/i18n/datepicker-$${language}.min.js \
					${SITEDIR}/include/js/LC_MESSAGES/jquery.ui.datepicker-$${file%.po}.js ; \
			fi ; \
		fi ; \
	done

	dh_link

override_dh_install:
	dh_install
	# Somewhere deep we don't want to install some files, remove them here
	cd ${SITEDIR}/include/vendor/flag-icon-css ; \
		rm -rf assets *.json Gruntfile.coffee index.html less LICENSE README.md sass svgo.yaml yarn.lock

	# Because we skip the install web page, we need to add some paths and other
	# items to the cacti settings. To avoid putting this in a patch, let's add
	# it here.
	cat debian/cacti_debian.sql >> ${SHAREDIR}/conf_templates/cacti.sql
	# Remove extra files as they aren't needed in Debian
	cd ${SITEDIR}/include/vendor/csrf && rm -f LICENSE NEWS README
